#!/bin/sh
cd scheduler/
rm -rf dist/
rm -rf build/
/usr/bin/gmake -f Makefile CONF=Release; make -f nbproject/Makefile-Release.mk;
rm -rf /usr/lib/libscheduler.so;
cp dist/Release/GNU-Linux/libscheduler.so /usr/lib;
 
ldconfig

echo '=================================================================='

cd ../lock_free_map/
rm -rf dist/
rm -rf build/
/usr/bin/gmake -f Makefile CONF=Release; make -f nbproject/Makefile-Release.mk;
rm -rf /usr/lib/liblock_free_map.so;
cp dist/Release/GNU-Linux/liblock_free_map.so /usr/lib;

echo '=================================================================='

cd ../lock_free_map_sk/
rm -rf dist/
rm -rf build/
/usr/bin/gmake -f Makefile CONF=Release; make -f nbproject/Makefile-Release.mk;
rm -rf /usr/lib/liblock_free_map_sk.so;
cp dist/Release/GNU-Linux/liblock_free_map_sk.so /usr/lib;

echo '=================================================================='

cd ../lock_free_multi_key_map/
rm -rf dist/
rm -rf build/
/usr/bin/gmake -f Makefile CONF=Release; make -f nbproject/Makefile-Release.mk;
rm -rf /usr/lib/liblock_free_multi_key_map.so;
cp dist/Release/GNU-Linux/liblock_free_multi_key_map.so /usr/lib;

echo '=================================================================='

cd ../non_blocking_queue/
rm -rf dist/
rm -rf build/
/usr/bin/gmake -f Makefile CONF=Release; make -f nbproject/Makefile-Release.mk;
rm -rf /usr/lib/libnon_blocking_queue.so;
cp dist/Release/GNU-Linux/libnon_blocking_queue.so /usr/lib;

echo '=================================================================='

cd ../set/
rm -rf dist/
rm -rf build/
/usr/bin/gmake -f Makefile CONF=Release; make -f nbproject/Makefile-Release.mk;
rm -rf /usr/lib/libset.so;
cp dist/Release/GNU-Linux/libset.so /usr/lib;

echo '=================================================================='

cd ../long_key_map/
rm -rf dist/
rm -rf build/
/usr/bin/gmake -f Makefile CONF=Release; make -f nbproject/Makefile-Release.mk;
rm -rf /usr/lib/liblong_key_map.so
cp dist/Release/GNU-Linux/liblong_key_map.so /usr/lib;

#cd ../string_kv_integer_map/
#rm -rf dist/
#rm -rf build/
#scl enable devtoolset-4 '/usr/bin/gmake -f Makefile CONF=Release; make -f nbproject/Makefile-Release.mk';
#rm -rf /usr/lib/libstring_kv_integer_map.so
#cp dist/Release/GNU-Linux/libstring_kv_integer_map.so /usr/lib;

echo '=================================================================='

#cd ../long_kv_integer_map/
#rm -rf dist/
#rm -rf build/
#scl enable devtoolset-4 '/usr/bin/gmake -f Makefile CONF=Release; make -f nbproject/Makefile-Release.mk';
#rm -rf /usr/lib/liblong_kv_integer_map.so
#cp dist/Release/GNU-Linux/liblong_kv_integer_map.so /usr/lib;

ldconfig