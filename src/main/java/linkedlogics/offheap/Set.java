/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap;

/**
 *
 * @author root
 */
public abstract class Set {

    static {
        try {
            System.load("/usr/lib/libset.so");
        } catch (Error er) {
            System.load("/root/NetBeansProjects/set/dist/Release/GNU-Linux/libset.so");
        }
    }

    protected native void add(int id, String s);

    protected native void add(int id, String s, long timeout);
//

    protected native void add(int id, long l);

    protected native void add(int id, long l, long timeout);

//    
    protected native void remove(int id, String s);

    protected native void remove(int id, long l);
//    

    protected native String next(int id);

    protected native long nextLong(int id);
//    

    protected native void resetIterator(int id);

    protected native void resetLongIterator(int id);
//    

    protected native boolean contains(int id, String s);

    protected native boolean contains(int id, long l);
//

    protected native void clear(int id);

    protected native void clearLong(int id);
//    

    protected native int create();

    protected native int createLong();

//    
    protected native int sizeLong(int id);

    protected native int size(int id);

}
