/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap;

import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author root
 */
public class StringKeyConcurrentHashMap {

    private final ReentrantLock lock = new ReentrantLock();

    static {
        try {
            System.load("/usr/lib/liblock_free_map_sk.so");
        } catch (Error er) {
            er.printStackTrace();
            System.load("/root/NetBeansProjects/lock_free_map_sk/dist/Debug/GNU-Linux/liblock_free_map_sk.so");
        }
    }

    private native boolean put(int id, String k, byte[] v, int version);

    private native boolean put(int id, String k, byte[] v, long expiry, int version);

    private native void setEvictionHandler(int id, EvictionNotificationHandler handler);

    private native byte[] get(int id, String k);

    private native void remove(int id, String k);

    private native void remove(int id, String k, int version);

    private native void suspendEviction(int id);

    private native void resumeEviction(int id);

    private native String consumeEvictions(int id);

    private native String consumeEvictionsNonBlocking(int id);

    private native int createMap();

    private native int createMap(int length);

    private native long size(int id);

    private native long sizeInBytes(int id);

    private native void reset(int id, int size);

    private final int id;

    public StringKeyConcurrentHashMap() {
        this.id = this.createMap();
    }

    public StringKeyConcurrentHashMap(int initSize) {
        this.id = this.createMap(initSize);
    }

    public boolean put(String key, byte[] value, int version) {
        if (key == null || key.trim().length() == 0) {
            return false;
        }
        if (value == null || value.length == 0) {
            return false;
        }

        lock.lock();
        try {
            boolean b = this.put(id, key, value, version);
            return b;
        } finally {
            lock.unlock();
        }
    }

    public boolean putUnsafe(String key, byte[] value, int version) {
        return this.put(id, key, value, version);
    }

    public void remove(String key) {
        if (key == null || key.trim().length() == 0) {
            throw new NullPointerException("Key cannot be null");
        }

        lock.lock();
        try {
            this.remove(id, key);
        } finally {
            lock.unlock();
        }
    }

    public void removeUnsafe(String key) {
        this.remove(id, key);
    }

    public void remove(String key, int version) {
        if (key == null || key.trim().length() == 0) {
            throw new NullPointerException("Key cannot be null");
        }

        lock.lock();
        try {
            this.remove(id, key, version);
        } finally {
            lock.unlock();
        }
    }

    public void removeUnsafe(String key, int version) {
        this.remove(id, key, version);
    }

    public byte[] get(String key) {
        if (key == null || key.trim().length() == 0) {
            throw new NullPointerException("Key cannot be null");
        }

        lock.lock();
        try {
            byte[] data = this.get(id, key);
            return data;
        } finally {
            lock.unlock();
        }
    }

    public byte[] getUnsafe(String key) {
        if (key == null || key.trim().length() == 0) {
            throw new NullPointerException("Key cannot be null");
        }

        byte[] data = this.get(id, key);
        return data;
    }

    public boolean put(String key, byte[] v, long expiry, int version) {
        if (key == null || key.trim().length() == 0) {
            throw new NullPointerException("Key cannot be null");
        }

        if (v == null || v.length == 0) {
            throw new NullPointerException("Value cannot be null");
        }

        lock.lock();
        try {
            boolean b = this.put(id, key, v, expiry, version);
            return b;
        } finally {
            lock.unlock();
        }
    }

    public boolean putUnsafe(String key, byte[] v, long expiry, int version) {
        return this.put(id, key, v, expiry, version);
    }

    public String consumeEvictions() {
        return this.consumeEvictions(id);
    }

    public String consumeEvictionsNonBlocking() {
        return this.consumeEvictionsNonBlocking(id);
    }

    public void resumeEviction() {
        this.resumeEviction(id);
    }

    public void suspendEviction() {
        this.suspendEviction(id);
    }

    public long size() {
        lock.lock();
        try {
            return this.size(id);
        } finally {
            lock.unlock();
        }
    }

    public long sizeInBytes() {
        lock.lock();
        try {
            return this.sizeInBytes(id);
        } finally {
            lock.unlock();
        }
    }

    public void reset(int size) {
        lock.lock();
        try {
            this.reset(id, size);
        } finally {
            lock.unlock();
        }
    }

    public void setEvictionHandler(EvictionNotificationHandler handler) {
        this.setEvictionHandler(id, new WrappedEvictionNotificationHandler(handler));
    }

    private final class WrappedEvictionNotificationHandler implements EvictionNotificationHandler {

        private EvictionNotificationHandler handler;

        public WrappedEvictionNotificationHandler(EvictionNotificationHandler handler) {
            this.handler = handler;
        }

        @Override
        public void onEvict(String key) {
            remove(key);
            handler.onEvict(key);
        }
    }

    public interface EvictionNotificationHandler {

        public void onEvict(String t);
    }
}
