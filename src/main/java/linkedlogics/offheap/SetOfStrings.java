/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap;

/**
 *
 * @author root
 */
public class SetOfStrings extends Set {

    private final int id;

    public SetOfStrings() {
        this.id = create();
    }

    public void add(String s) {
        if (s == null || s.trim().length() == 0) {
            throw new NullPointerException("Parameter can not be null");
        }
        add(id, s);
    }

    public void remove(String s) {
        if (s == null || s.trim().length() == 0) {
            throw new NullPointerException("Parameter can not be null");
        }
        remove(id, s);
    }

    public String next() {
        return next(id);
    }

    public void allocIterator() {
        resetIterator(id);
    }

    public boolean contains(String s) {
        if (s == null || s.trim().length() == 0) {
            throw new NullPointerException("Parameter can not be null");
        }
        return contains(id, s);
    }

    public void resetIterator() {
        resetIterator(id);
    }

    public void clear() {
        clear(id);
    }

    public int size() {
        return size(id);
    }
}
