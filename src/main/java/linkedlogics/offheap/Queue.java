/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap;

/**
 *
 * @author root
 */
public class Queue {

    static {
        try {
            System.load("/usr/lib/libnon_blocking_queue.so");
        } catch (Error ex) {
            ex.printStackTrace();
            System.load("/root/NetBeansProjects/non_blocking_queue/dist/Debug/GNU-Linux/libnon_blocking_queue.so");
        }
    }

    private native void offer(int queue_id, byte[] data);

    private native byte[] peek(int queue_id);

    private native byte[] poll(int queue_id);

    private native long size(int queue_id);

    private native long sizeInBytes(int queue_id);

    private native int getMax(int queue_id);

    private native int getMaxFromStart(int queue_id);

    private native int getAverage(int queue_id);

    private native int createQueue();

    protected final int index;

    public Queue() {
        index = this.createQueue();
    }

    public void offer(byte[] data) {
        if (data == null || data.length == 0) {
            throw new NullPointerException("Parameter can not be null");
        }
        this.offer(index, data);
    }

    public byte[] peek() throws InterruptedException {
        return this.peek(index);
    }

    public long size() {
        return this.size(index);
    }

    public long sizeInBytes() {
        return this.sizeInBytes(index);
    }

    public byte[] poll() throws InterruptedException {
        return this.poll(index);
    }

    /**
     *
     * @return array of statistics. First average, Second Max from start, Third
     * Maximum.
     */
    public int[] getStatistics() {
        int i[] = {this.getAverage(index), this.getMaxFromStart(index), this.getMax(index)};
        return i;
    }
}
