/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap;

/**
 *
 * @author root
 */
public class ConcurrentHashMap {

    static {
//        System.load("/root/NetBeansProjects/lock_free_map_1/dist/Debug/GNU-Linux/liblock_free_map_1.so");
        try {
            System.load("/usr/lib/liblock_free_map.so");
        } catch (Error ex) {
            ex.printStackTrace();
            System.load("/root/NetBeansProjects/lock_free_map/dist/Debug/GNU-Linux/liblock_free_map.so");
        }
    }

    private native void setEvictionHandler(int id, EvictionNotificationHandler handler);

    private native boolean put(int id, long k, byte[] v, int version);

    private native boolean put(int id, long k, byte[] v, long timeout, int version);

    private native byte[] get(int id, long k);

    private native int tryLock(int id, long k, long t);

    private native int lock(int id, long k, long t);

    private native int lock(int id, long k, long t, int lockId);

    private native boolean unlock(int id, long k, int v);

    private native boolean remove(int id, long k);

    private native boolean remove(int id, long k, int version);

    private native boolean rescheduleEviction(int id, long k, long timeout);

    private native int createMap();

    private native int createMap(int length);

    private native void begin(int id);

    private native void suspendEviction(int id);

    private native void resumeEviction(int id);

    private native long next(int id);

    private native boolean hasMore(int id);

    private native void printStat(int id);

    private native long evictedCount(int id);

    private native int size(int id);

    private native long sizeInBytes(int id);

    private native long consumeEviction(int id);

    private native long consumeEvictionNonBlocking(int id);

    private native void destroy(int id);

    private native int checkLock(int id, long key, int lock_id);

    private final int id;

    public ConcurrentHashMap(int initSize) {
        this.id = this.createMap(initSize);
    }

//    public byte[] getAndLock(long key, long timeout) {
//        int result = this.lock(id, key, timeout);
//        if (result >= 0) {
//            return this.get(id, key);
//        }
//        return null;
//    }
    public boolean putAndUnlock(long key, byte[] value, int version, int lock_id) {
        if (value == null || value.length == 0) {
            throw new NullPointerException("Parameter value can not be null");
        }

        int lockCheckResult = 0;
        if (lock_id <= 0) {
            lockCheckResult = 1;
        } else {
            lockCheckResult = checkLock(id, key, lock_id);
        }

        if (lockCheckResult > 0) {
            boolean result = this.put(id, key, value, version);
//            System.out.println("PutResult = " + result);
            return result & (lock_id > 0 ? this.unlock(id, key, lock_id) : true);
        } else {
//            System.out.println("lock check result is " + lockCheckResult);
        }
        return false;
    }

    public void destroy() {
        this.destroy(id);
    }

    public boolean put(long key, byte[] value, int version) {
        if (value == null || value.length == 0) {
            throw new NullPointerException("Parameter value can not be null. Key [" + key + "]");
        }
        return this.put(id, key, value, version);
    }

    public boolean put(long key, byte[] value, long expire, int version) {
        if (value == null || value.length == 0) {
            throw new NullPointerException("Parameter value can not be null. Key [" + key + "]");
        }
        return this.put(id, key, value, expire, version);
    }

    public int tryLock(long key, long timeout) {
        return this.tryLock(id, key, timeout);
    }

    public boolean remove(long key) {
        return this.remove(id, key);
    }

    public boolean remove(long key, int version) {
        return this.remove(id, key, version);
    }

    public byte[] get(long key) {
        return this.get(id, key);
    }

    public int lock(long key, long timeout) {
        return this.lock(id, key, timeout);
    }

    //Distribute lock
    public int lock(long key, long timeout, int lockId) {
        if (lockId == 0) {
            return lock(key, timeout);
        }
        return this.lock(id, key, timeout, lockId);
    }

    public boolean unlock(long key, int lock_id) {
        return this.unlock(id, key, lock_id);
    }

    public void resumeEviction() {
        this.resumeEviction(id);
    }

    public void suspendEviction() {
        this.suspendEviction(id);
    }

    public void rescheduleEviction(long key, long timeout) {
        this.rescheduleEviction(id, key, timeout);
    }

    public long consumeEviction() {
        return this.consumeEviction(id);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
        destroy(id);

    }

    public Long consumeEvictionNonBlocking() {
        long k = this.consumeEvictionNonBlocking(id);
        if (k < 0) {
            return null;
        }
        return k;
    }

    public int size() {
        return this.size(id);
    }

    public long sizeInBytes() {
        return this.sizeInBytes(id);
    }

    public long getEvictedCount() {
        return this.evictedCount(id);
    }

    public Iterator iterator() {
        this.begin(id);
        return new Iterator();
    }

    public void printStat() {
        this.printStat(id);
    }

    public void setEvictionHandler(EvictionNotificationHandler handler) {
        this.setEvictionHandler(id, new WrappedEvictionNotificationHandler(handler));
    }

    private final class WrappedEvictionNotificationHandler implements EvictionNotificationHandler {

        private EvictionNotificationHandler handler;

        public WrappedEvictionNotificationHandler(EvictionNotificationHandler handler) {
            this.handler = handler;
        }

        @Override
        public void onEvict(long key) {
            handler.onEvict(key);
        }
    }

    public interface EvictionNotificationHandler {

        public void onEvict(long t);
    }

    public class Iterator {

        public long next() {
            return ConcurrentHashMap.this.next(id);
        }

        public boolean hasMore() {
            return ConcurrentHashMap.this.hasMore(id);
        }
    }
}
