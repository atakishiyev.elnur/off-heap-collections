/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap.test;

import java.io.IOException;
import linkedlogics.offheap.ConcurrentMultiKeyMap;
import linkedlogics.offheap.Queue;

/**
 *
 * @author root
 */
public class QueueTest {

    public static void main(String[] args) throws InterruptedException, IOException {

        final Queue blockingQueue = new Queue();
        int i = 0;

        ConcurrentMultiKeyMap cmkm = new ConcurrentMultiKeyMap();
        cmkm.put(100, 2, 3, "HelloWorld1".getBytes(), 0);
        cmkm.remove(100, 2, 3);

        System.out.println(cmkm.get(100, 2, 3));
        
        cmkm.put(200, 2, 3, "HelloWorld2".getBytes(), 1);
        cmkm.remove(-1, 2, 3);
        
        System.out.println(new String(cmkm.get(-1, 2, -1)));
//        System.in.read();
//        System.out.println("Size = " + blockingQueue.sizeInBytes());
//        System.out.println(new String(data));
//
////        data = blockingQueue.poll();
////        System.out.println(new String(data));
//        int[] stat = blockingQueue.getStatistics();
//        System.out.println("Average = " + stat[0] + ";" + "Max From Start = " + stat[1] + ";Max = " + stat[2]);
//
//        stat = blockingQueue.getStatistics();
//        System.out.println("Average = " + stat[0] + ";" + "Max From Start = " + stat[1] + ";Max = " + stat[2]);
    }
}
