/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap.test;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import linkedlogics.offheap.BlockingQueue;
import linkedlogics.offheap.LongKeyConcurrentHashMap;
import linkedlogics.offheap.SetOfStrings;

/**
 *
 * @author root
 */
public class testLongKeyMap {

    public static void main(String[] args) throws InterruptedException, IOException {
        final LongKeyConcurrentHashMap lk = new LongKeyConcurrentHashMap(100);
        Executors.newCachedThreadPool().submit(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    long k = lk.consumeEvictions();
                    System.out.println(k);
                }
            }
        });
        lk.put(1, "HeloWorld".getBytes(), 5, 0);
        lk.put(2, "HeloWorld".getBytes(), 5, 0);
        lk.put(3, "HeloWorld".getBytes(), 5, 0);
        lk.put(4, "HeloWorld".getBytes(), 5, 0);
        lk.put(5, "HeloWorld".getBytes(), 5, 0);
        lk.put(6, "HeloWorld".getBytes(), 5, 0);
        lk.put(7, "HeloWorld".getBytes(), 5, 0);

//        System.out.println(new String(lk.get(1)));

//        lk.reset(120);

//        lk.put(1, "HeloWorld".getBytes(), 0);
//        System.out.println(new String(lk.get(1)));
    }
}
