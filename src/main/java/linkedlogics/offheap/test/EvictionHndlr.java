/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap.test;

import java.util.concurrent.locks.ReentrantLock;
import linkedlogics.offheap.ConcurrentHashMap;

/**
 *
 * @author root
 */
public class EvictionHndlr implements ConcurrentHashMap.EvictionNotificationHandler {

    ConcurrentHashMap map;
    int evictionCount = 0;
    ReentrantLock lock = new ReentrantLock();

    public EvictionHndlr(ConcurrentHashMap map) {
        this.map = map;
    }

    
    @Override
    public void onEvict(long t) {
        byte[] data = map.get(t);
        map.remove(t);
        map.put(t, data, 3, 1);
        evictionCount++;
        System.out.println("EvictedCount = " + evictionCount + " KeyEvicted = " + t);
    }

}
