/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap.test;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import linkedlogics.offheap.ConcurrentMultiKeyMap;

/**
 *
 * @author root
 */
public class testConcurrentMultiKeyHashmap {

    public static void main(String[] args) throws IOException, InterruptedException {
        final ConcurrentMultiKeyMap map = new ConcurrentMultiKeyMap(5000000);

        int j = 30;
        map.put(j, j * 10000, j * 50000, "Hello".getBytes(), 0);

        int lock_id = map.lock(j, j, j, 140);
        System.out.println("LockId" + lock_id);
        boolean b = map.unlock(j, j, j, lock_id);
        System.out.println(b);
        b = map.unlock(j, j, j, lock_id);
        System.out.println(b);
        map.unlock(j, j, j, lock_id);
        map.unlock(j, j, j, lock_id);

        for (int i = 0; i < 3; i++) {
            Executors.newSingleThreadExecutor().submit(new Runnable() {
                @Override
                public void run() {
                    for (int i = 1; i < 5000; i++) {
                        map.put(i, i * 10000, i * 50000, "Hello".getBytes(), 0);
                    }

                    System.out.println("finished");
                    final int lock_id = map.lock(3, 0, 0, 60);
                    map.put(3, 3 * 10000, 3 * 50000, "hello1".getBytes(), 0);
                    System.out.println("Size = " + map);
                    Executors.newScheduledThreadPool(1).schedule(new Runnable() {
                        @Override
                        public void run() {
                            map.unlock(3, 0, 0, lock_id);
                            System.out.println("unlocked");
                        }
                    }, 30, TimeUnit.SECONDS);
                    System.out.println(new String(map.get(3, 0, 0)));

                    Executors.newSingleThreadExecutor().submit(new Runnable() {
                        @Override
                        public void run() {
                            map.lock(3, 0, 0, 0);
                            map.put(3, 3 * 10000, 3 * 50000, "Hello4".getBytes(), 0);
                            System.out.println("Done");
                        }
                    });

                    Executors.newSingleThreadExecutor().submit(new Runnable() {
                        @Override
                        public void run() {
                            map.lock(15, 0, 0, 50);
                            map.put(3, 3 * 10000, 3 * 50000, "dddd".getBytes(), 0);
                        }
                    });

                }
            });
        }

    }
}
