/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap.test;

import linkedlogics.offheap.StringKeyConcurrentHashMap;

/**
 *
 * @author root
 */
public class testConcurrentHashmap {

    public static void main(String[] args) {  // prepare test data  

        StringKeyConcurrentHashMap stringKeyConcurrentHashMap = new StringKeyConcurrentHashMap(10);
        stringKeyConcurrentHashMap.put("HelloWorld", "Test".getBytes(), 0);

        stringKeyConcurrentHashMap.reset(15);
        System.out.println("Getting value");
        System.out.println(stringKeyConcurrentHashMap.get("HelloWorld"));
        stringKeyConcurrentHashMap.put("HelloWorld", "Test".getBytes(), 0);
        System.out.println(new String(stringKeyConcurrentHashMap.get("HelloWorld")));
        System.out.println("Value got");

    }
}
