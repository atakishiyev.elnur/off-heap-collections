/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author root
 */
public class BlockingQueue extends Queue {

    private final ReentrantLock lock = new ReentrantLock();
    private final Condition condition = lock.newCondition();

    public BlockingQueue() {
        super();
    }

    @Override
    public void offer(byte[] data) {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            super.offer(data); //To change body of generated methods, choose Tools | Templates.
            condition.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public byte[] peek() throws InterruptedException {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            return super.peek();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public byte[] poll() throws InterruptedException {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            return super.poll();
        } finally {
            lock.unlock();
        }
    }

    public byte[] take() throws InterruptedException {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            byte[] data = null;
            while (true) {
                data = super.poll();
                if (data == null) {
                    condition.await();
                    data = super.poll();
                }
                return data;
            }

        } finally {
            lock.unlock();
        }
    }
}
