/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap;

import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author root
 */
public class LongKeyConcurrentHashMap {

    private final ReentrantLock lock = new ReentrantLock();

    static {
        try {
            System.load("/usr/lib/liblong_key_map.so");
        } catch (Error er) {
            er.printStackTrace();
            System.load("/root/NetBeansProjects/long_key_map/dist/Debug/GNU-Linux/liblong_key_map.so");
        }
    }

    private native boolean put(int id, long k, byte[] v, int version);

    private native boolean put(int id, long k, byte[] v, long expiry, int version);

    private native byte[] get(int id, long k);

    private native boolean remove(int id, long k);

    private native boolean remove(int id, long k, int version);

    private native void suspendEviction(int id);

    private native void resumeEviction(int id);

    private native long consumeEvictions(int id);

    private native long consumeEvictionsNonBlocking(int id);

    private native int createMap();

    private native int createMap(int length);

    private native long size(int id);

    private native long sizeInBytes(int id);

    private native void reset(int id, int size);

    private final int id;

    public LongKeyConcurrentHashMap(int size) {
        this.id = createMap(size);
    }

    public Long consumeEvictionsNonBlocking() {
        long k = consumeEvictionsNonBlocking(id);
        return k >= 0 ? k : null;
    }

    public long consumeEvictions() {
        return consumeEvictions(id);
    }

    public void resumeEviction() {
        lock.lock();
        try {
            resumeEviction(id);
        } finally {
            lock.unlock();
        }
    }

    public void suspendEviction() {
        lock.lock();
        try {
            suspendEviction(id);
        } finally {
            lock.unlock();
        }
    }

    public boolean remove(long k, int version) {
        lock.lock();
        try {
            return remove(id, k, version);
        } finally {
            lock.unlock();
        }
    }

    public boolean removeUnsafe(long k, int version) {
        return remove(id, k, version);
    }

    public boolean remove(long k) {
        lock.lock();
        try {
            return this.remove(id, k);
        } finally {
            lock.unlock();
        }
    }

    public boolean removeUnsafe(long k) {
        return this.remove(id, k);
    }

    public boolean put(long k, byte[] data, int version) {
        lock.lock();
        try {
            return put(id, k, data, version);
        } finally {
            lock.unlock();
        }
    }

    public boolean putUnsafe(long k, byte[] data, int version) {
        return put(id, k, data, version);
    }

    public boolean put(long k, byte[] data, long expiry, int version) {
        lock.lock();
        try {
            return put(id, k, data, expiry, version);
        } finally {
            lock.unlock();
        }
    }

    public boolean putUnsafe(long k, byte[] data, long expiry, int version) {
        return put(id, k, data, expiry, version);
    }

    public byte[] get(long k) {
        lock.lock();
        try {
            return get(id, k);
        } finally {
            lock.unlock();
        }
    }

    public byte[] getUnsafe(long k) {
        return get(id, k);
    }

    public void reset(int size) {
        reset(id, size);
    }

    public long size() {
        lock.lock();
        try {
            return size(id);
        } finally {
            lock.unlock();
        }
    }

    public long sizeInBytes() {
        lock.lock();
        try {
            return sizeInBytes(id);
        } finally {
            lock.unlock();
        }
    }
}
