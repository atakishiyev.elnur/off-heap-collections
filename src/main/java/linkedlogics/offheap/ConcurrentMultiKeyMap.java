/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap;

/**
 *
 * @author root
 */
public class ConcurrentMultiKeyMap {

    static {
        try {
            System.load("/usr/lib/liblock_free_multi_key_map.so");
        } catch (Error er) {
            er.printStackTrace();
            System.load("/root/NetBeansProjects/lock_free_multi_key_map/dist/Debug/GNU-Linux/liblock_free_multi_key_map.so");
        }
    }

    private native boolean put(int id, long key1, long key2, long key3, byte[] data, long version);

    private native boolean put(int id, long key1, long key2, long key3, byte[] data, long version, long timeout);

    private native boolean putAndUnLock(int id, long key1, long key2, long key3, byte[] data, long version, int lock_id);

    private native boolean putAndUnLock(int id, long key1, long key2, long key3, byte[] data, long version, long timeout, int lock_id);

    private native byte[] get(int id, long key1, long key2, long key3);

    private native byte[] getAndLock(int id, long key1, long key2, long key3, long timeout);

    private native int tryLock(int id, long key1, long key2, long key3, long t);

    private native int lock(int id, long key1, long key2, long key3, long timeout);

    private native int lock(int id, long key1, long key2, long key3, int lockId, long timeout);

    private native boolean unlock(int id, long key1, long key2, long key3, int lock_id);

    private native void remove(int id, long key1, long key2, long key3);

    private native long consumeEvictionBlocking(int id);

    private native long consumeEviction(int id);

    private native int createMap();

    private native int createMap(int length);

    private native void destroy(int id);

    private final int id;

    public ConcurrentMultiKeyMap() {
        this.id = this.createMap();
    }

    public ConcurrentMultiKeyMap(int initialSize) {
        this.id = this.createMap(initialSize);
    }

    public boolean put(long k1, long k2, long k3, byte[] value, int version) {
        if (value == null || value.length == 0) {
            throw new NullPointerException("Parameter value can not be null");
        }
        return this.put(id, k1, k2, k3, value, version);
    }

    public boolean putAndUnlock(long k1, long k2, long k3, byte[] data, int version, int lockId) {
        if (data == null || data.length == 0) {
            throw new NullPointerException("Parameter value can not be null");
        }
        return this.putAndUnLock(id, k1, k2, k3, data, version, lockId);
    }

    public boolean putAndUnlock(long k1, long k2, long k3, byte[] data, int version, long timeout, int lockId) {
        if (data == null || data.length == 0) {
            throw new NullPointerException("Parameter value can not be null");
        }
        return this.putAndUnLock(id, k1, k2, k3, data, version, timeout, lockId);
    }

    public byte[] get(long k1, long k2, long k3) {
        return this.get(id, k1, k2, k3);
    }

//    public byte[] getAndLock(long k1, long k2, long k3, long timeout) {
//        return this.getAndLock(id, k1, k2, k3, timeout);
//    }
    public int tryLock(long key1, long key2, long key3, long timeout) {
        return this.tryLock(id, key1, key2, key3, timeout);
    }

    public int lock(long k1, long k2, long k3, long timeout) {
        return this.lock(id, k1, k2, k3, timeout);
    }

    public int lock(long key1, long key2, long key3, int lockId, long timeout) {
        if (lockId == 0) {
            return lock(key1, key2, key3, timeout);
        }
        return this.lock(id, key1, key2, key3, lockId, timeout);
    }

    public boolean unlock(long k1, long k2, long k3, int lock_id) {
        return this.unlock(id, k1, k2, k3, lock_id);
    }

    public void remove(long k1, long k2, long k3) {
        this.remove(id, k1, k2, k3);
    }

    public Long consumeEviction() {
        long k = this.consumeEviction(id);
        if (k < 0) {
            return null;
        }
        return k;
    }

    public Long consumeEvictionBlocking() {
        return this.consumeEvictionBlocking(id);
    }

    public boolean put(long key1, long key2, long key3, byte[] data, long version, long timeout) {
        return this.put(id, key1, key2, key3, data, version, timeout);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
//        this.destroy(id);
    }

}
