/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap.deprecated;

import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author root
 */
public class LongKVIntegerMap {

    ReentrantLock _lock = new ReentrantLock();

    static {
//        System.load("/root/NetBeansProjects/long_kv_integer_map/dist/Debug/GNU-Linux/liblong_kv_integer_map.so");
        System.load("/usr/lib/liblong_kv_integer_map.so");
    }

    private native boolean put(int id, long k, int v, long expire);

    private native int get(int id, long k);

    private native boolean remove(int id, long k);

    private native int create(int size);

    private native void begin(int id);

    private native int next(int id);

    private native void setEvictionHandler(int id, EvictionHandler handler);

    private native boolean hasMore(int id);

    private native int size(int id);

    private final int id;

    private LongKVIntegerMap(int size) {
        this.id = this.create(size);
    }

    public boolean put(long k, int v, long expire) {
//        final ReentrantLock __lock = this._lock;
//        try {
//            __lock.lock();
        return this.put(id, k, v, expire);
//        } finally {
//            __lock.unlock();
//        }
    }

    public int get(long k) {
//        final ReentrantLock __lock = this._lock;
//        try {
//            __lock.lock();
        return this.get(id, k);
//        } finally {
//            __lock.unlock();
//        }
    }

    public boolean remove(long k) {
//        final ReentrantLock __lock = this._lock;
//        try {
//            __lock.lock();
        return this.remove(id, k);
//        } finally {
//            __lock.unlock();
//        }
    }

    public Iterator iterator() {
//        final ReentrantLock __lock = this._lock;
//        try {
//            __lock.lock();
        this.begin(id);
        return new Iterator();
//        } finally {
//            __lock.unlock();
//        }
    }

    public void setEvictionHandler(EvictionHandler handler) {
        this.setEvictionHandler(id, handler);
    }

    public interface EvictionHandler {

        public void onEvict(long k);
    }

    public class Iterator {

        public int next() {
//            final ReentrantLock __lock = _lock;
//            try {
//                __lock.lock();
            return LongKVIntegerMap.this.next(id);
//            } finally {
//                __lock.unlock();
//            }
        }

        public boolean hasMore() {
//            final ReentrantLock __lock = _lock;
//            try {
//                __lock.lock();
            return LongKVIntegerMap.this.hasMore(id);
//            } finally {
//                __lock.unlock();
//            }
        }
    }

    public int size() {
//        final ReentrantLock __lock = _lock;
//        try {
//        __lock.lock();
        return size(id);
//        } finally {
//            __lock.unlock();
//        }
    }
}
