/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap.deprecated;

import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author root
 */
public class StringKVIntMap {

    final ReentrantLock _lock = new ReentrantLock();

    static {
//        System.load("/root/NetBeansProjects/string_kv_integer_map/dist/Debug/GNU-Linux/libstring_kv_integer_map.so");
        System.load("/usr/lib/libstring_kv_integer_map.so");
    }

    private native boolean put(int id, String k, int v, long expire);

    private native int get(int id, String k);

    private native boolean remove(int id, String k);

    private native int create(int size);

    private native void begin(int id);

    private native String next(int id);

    private native void setEvictionHandler(int id, EvictionHandler handler);

    private native boolean hasMore(int id);

    private native int size(int id);

    private final int id;

    private StringKVIntMap(int size) {
        this.id = this.create(size);
    }

    public boolean put(String k, int v, long expire) {
        if (k == null) {
            return false;
        }

        if (k.trim().equals("")) {
            return false;
        }

//        final ReentrantLock __lock = _lock;
//        try {
//            __lock.lock();
        return this.put(id, k, v, expire);
//        } finally {
//            __lock.unlock();
//        }
    }

    public int get(String k) {
//        final ReentrantLock __lock = _lock;
//        try {
//            __lock.lock();
        return this.get(id, k);
//        } finally {
//            __lock.unlock();
//        }
    }

    public boolean remove(String k) {
//        final ReentrantLock __lock = _lock;
//        try {
//            __lock.lock();
        return this.remove(id, k);
//        } finally {
//            __lock.unlock();
//        }
    }

    public Iterator iterator() {
//        final ReentrantLock __lock = _lock;
//        try {
//            __lock.lock();
        this.begin(id);
        return new Iterator();
//        } finally {
//            __lock.unlock();
//        }
    }

    public void setEvictionHandler(EvictionHandler handler) {
        this.setEvictionHandler(id, handler);
    }

    public interface EvictionHandler {

        public void onEvict(String key);
    }

    public class Iterator {

        public String next() {
//            final ReentrantLock __lock = _lock;
//            try {
//                __lock.lock();
            return StringKVIntMap.this.next(id);
//            } finally {
//                __lock.unlock();
//            }
        }

        public boolean hasMore() {
//            final ReentrantLock __lock = _lock;
//            try {
//                __lock.lock();
            return StringKVIntMap.this.hasMore(id);
//            } finally {
//                __lock.unlock();
//            }
        }
    }

    public int size() {
//        final ReentrantLock __lock = _lock;
//        try {
//            __lock.lock();
        return size(id);
//        } finally {
//            __lock.unlock();
//        }
    }
}
