/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedlogics.offheap;

/**
 *
 * @author root
 */
public class SetOfLongs extends Set {

    private final int id;

    public SetOfLongs() {
        this.id = createLong();
    }

    public void add(long s) {
        add(id, s);
    }

    public void remove(long s) {
        remove(id, s);
    }

    public long next() {
        return nextLong(id);
    }

    public void allocIterator() {
        resetLongIterator(id);
    }

    public boolean contains(long s) {
        return contains(id, s);
    }

    public void resetIterator() {
        resetLongIterator(id);
    }

    public void clear() {
        clearLong(id);
    }
    
    public int size(){
        return sizeLong(id);
    }

}
